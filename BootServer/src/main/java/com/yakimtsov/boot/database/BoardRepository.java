package com.yakimtsov.boot.database;

import com.yakimtsov.boot.entity.Board;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board, Long> {
}
