package com.yakimtsov.boot.database;

import com.yakimtsov.boot.entity.Stage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StageRepository extends JpaRepository<Stage, Long> {
    List<Stage> findByBoardId(long id);
}
