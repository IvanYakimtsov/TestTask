package com.yakimtsov.boot.controller;

import com.yakimtsov.boot.database.BoardRepository;
import com.yakimtsov.boot.database.StageRepository;
import com.yakimtsov.boot.database.TaskRepository;
import com.yakimtsov.boot.entity.Board;
import com.yakimtsov.boot.entity.Stage;
import com.yakimtsov.boot.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("trellolo")
public class Controller {
    private BoardRepository boardRepository;
    private StageRepository stageRepository;
    private TaskRepository taskRepository;


    @Autowired
    public Controller(BoardRepository boardRepository, StageRepository stageRepository, TaskRepository taskRepository) {
        this.boardRepository = boardRepository;
        this.stageRepository = stageRepository;
        this.taskRepository = taskRepository;
    }


    @RequestMapping(value = "boards", method = RequestMethod.GET)
    public ResponseEntity<List<Board>> getArticles() {
        List<Board> boards = boardRepository.findAll();
        return new ResponseEntity<>(boards, HttpStatus.OK);
    }

    @RequestMapping(value = "boards/{id}", method = RequestMethod.GET)
    public ResponseEntity<Board> getBoard(@PathVariable("id") String id) {

        Long boardId = Long.valueOf(id);
        Optional<Board> optionalBoard = boardRepository.findById(boardId);
        if (optionalBoard.isPresent()) {
            Board board = optionalBoard.get();
            List<Stage> stages = buildBoardInfo(boardId);
            board.setStages(stages);
            return new ResponseEntity<>(board, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "boards/add", method = RequestMethod.PUT)
    public ResponseEntity<Void> addBoard(@RequestBody Board board) {
        boardRepository.save(board);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "boards/update", method = RequestMethod.POST)
    public ResponseEntity<Void> updateBoard(@RequestBody Board board) {
        boardRepository.save(board);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "boards/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteBoard(@PathVariable("id") String id) {
        Long boardId = Long.valueOf(id);
        boardRepository.deleteById(boardId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "stages/add", method = RequestMethod.PUT)
    private ResponseEntity<Void> addStage(@RequestBody Stage stage) {
        stageRepository.save(stage);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "stages/update", method = RequestMethod.POST)
    public ResponseEntity<Void> updateStage(@RequestBody Stage stage) {
        stageRepository.save(stage);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "stages/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteStage(@PathVariable("id") String id) {
        Long stageId = Long.valueOf(id);
        stageRepository.deleteById(stageId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "tasks/add", method = RequestMethod.PUT)
    private ResponseEntity<Void> addTask(@RequestBody Task task) {
        taskRepository.save(task);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "tasks/update", method = RequestMethod.POST)
    public ResponseEntity<Void> updateStage(@RequestBody Task task) {
        taskRepository.save(task);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "tasks/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTask(@PathVariable("id") String id) {
        Long taskId = Long.valueOf(id);
        taskRepository.deleteById(taskId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    private List<Stage> buildBoardInfo(long boardId) {
        List<Stage> stages = stageRepository.findByBoardId(boardId);
        stages.forEach(stage -> stage.setTasks(taskRepository.findByStageId(stage.getId())));
        return stages;
    }
}