<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<html>
<head><title>Hello</title></head>
<body>
<H1><B>Welcome!</B></H1>
<br>
<form action="${pageContext.request.contextPath}/addBoard" method="post">
    <input type="text" name="description">
    <input type='submit' value='Create board'/>
</form>
<br>
<c:forEach items="${list}" var="item">
    <tr>
        <a href="${pageContext.request.contextPath}/showBoard?id=${item.id}">${item.description}</a>
    </tr>

    <br/>
</c:forEach>
</body>
</html>
