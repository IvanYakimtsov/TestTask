<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<html>
<head><title>Hello</title>
    <style>
        .brd {
            border: 2px solid black;
            padding: 10px;
            margin: 10px;
        }
    </style>
</head>
<body>
<div class="brd">
    
    <h1>Board : </h1>
    <form action="${pageContext.request.contextPath}/updateBoard" method="post">
        <input type="text" name="description" value="${entity.description}">
        <input type="hidden" name="boardId" value="${entity.id}">
        <input type='submit' value='rename'/>
    </form>
    <form action="${pageContext.request.contextPath}/deleteBoard" method="post">
        <input type="hidden" name="boardId" value="${entity.id}">
        <input type='submit' value='delete'/>
    </form>
    <br>
    <form action="${pageContext.request.contextPath}/addStage" method="post">
        <input type="text" name="description">
        <input type="hidden" name="boardId" value="${entity.id}">
        <input type='submit' value='Create stage'/>
    </form>

    <h2>Stages:</h2>
    <br>
    <c:forEach items="${entity.stages}" var="stage">
        <div class="brd">
            <h2>${stage.description}</h2>
            <form action="${pageContext.request.contextPath}/deleteStage" method="post">
                <input type="hidden" name="boardId" value="${entity.id}">
                <input type="hidden" name="stageId" value="${stage.id}">
                <input type='submit' value='delete'/>
            </form>

            <br/>
            <form action="${pageContext.request.contextPath}/addTask" method="post">
                <input type="text" name="description">
                <input type="hidden" name="boardId" value="${entity.id}">
                <input type="hidden" name="stageId" value="${stage.id}">
                <input type='submit' value='Create task'/>
            </form>
            <br>
            <div class="brd">
                Tasks:
                <br>
                <c:forEach items="${stage.tasks}" var="task">
                    ${task.description}
                    <form action="${pageContext.request.contextPath}/deleteTask" method="post">
                        <input type="hidden" name="boardId" value="${entity.id}">
                        <input type="hidden" name="taskId" value="${task.id}">
                        <input type='submit' value='delete'/>
                    </form>

                    <br/>
                </c:forEach>
                <br/>
            </div>
        </div>
    </c:forEach>
</div>
<form action="${pageContext.request.contextPath}/home">
    <input type='submit' value='home'/>
</form>
</body>
</html>
