package com.yakimtsov.bootclient.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Task {
    private long id;
    private String description;
    private long stageId;
    private long boardId;
}
