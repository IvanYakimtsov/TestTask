package com.yakimtsov.bootclient.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class Stage {
    private long id;
    private String description;
    private long boardId;
    private List<Task> tasks;
}
