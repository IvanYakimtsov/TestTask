package com.yakimtsov.bootclient.controller;

import com.yakimtsov.bootclient.entity.Board;
import com.yakimtsov.bootclient.entity.Stage;
import com.yakimtsov.bootclient.entity.Task;
import com.yakimtsov.bootclient.rest.RestConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class AppController {
    private final RestConnector restConnector;

    @Autowired
    public AppController(RestConnector restConnector) {
        this.restConnector = restConnector;
    }

    @RequestMapping(value ="/home", method = RequestMethod.GET)
    public ModelAndView findBoards() {
        List<Board> boards = restConnector.findBoards();
        return new ModelAndView("/pages/homePage.jsp", "list", boards);
    }

    @RequestMapping(value = "/showBoard", method = RequestMethod.GET)
    public ModelAndView showBoard(@RequestParam String id){
        Board board = restConnector.findBoard(id);
        return new ModelAndView("/pages/boardPage.jsp", "entity", board);
    }

    @RequestMapping(value = "/addBoard", method = RequestMethod.POST)
    public ModelAndView addBoard(@RequestParam String description){
        Board board = new Board();
        board.setDescription(description);
        restConnector.addBoard(board);
        return new ModelAndView("redirect:/home");
    }

    @RequestMapping(value = "/addStage", method = RequestMethod.POST)
    public ModelAndView addStage(@RequestParam String description, @RequestParam long boardId){
        Stage stage = new Stage();
        stage.setDescription(description);
        stage.setBoardId(boardId);
        restConnector.addStage(stage);
        return new ModelAndView("redirect:/showBoard?id=" + boardId);
    }

    @RequestMapping(value = "/addTask", method = RequestMethod.POST)
    public ModelAndView addTask(@RequestParam String description,@RequestParam long stageId,
                                @RequestParam long boardId){
        Task task = new Task();
        task.setDescription(description);
        task.setBoardId(boardId);
        task.setStageId(stageId);
        restConnector.addTask(task);
        return new ModelAndView("redirect:/showBoard?id=" + boardId);
    }

    @RequestMapping(value = "/deleteStage", method = RequestMethod.POST)
    public ModelAndView deleteStage(@RequestParam String boardId, @RequestParam String stageId){
        restConnector.deleteStage(stageId);
        return new ModelAndView("redirect:/showBoard?id=" + boardId);
    }

    @RequestMapping(value = "/deleteTask", method = RequestMethod.POST)
    public ModelAndView deleteTask(@RequestParam String boardId, @RequestParam String taskId){
        restConnector.deleteTask(taskId);
        return new ModelAndView("redirect:/showBoard?id=" + boardId);
    }

    @RequestMapping(value = "/deleteBoard", method = RequestMethod.POST)
    public ModelAndView deleteBoard(@RequestParam String boardId){
        restConnector.deleteBoard(boardId);
        return new ModelAndView("redirect:/home");
    }

    @RequestMapping(value = "/updateBoard", method = RequestMethod.POST)
    public ModelAndView updateBoard(@RequestParam long boardId,@RequestParam String description){
        if(!description.isEmpty()){
            Board board = new Board();
            board.setId(boardId);
            board.setDescription(description);
            restConnector.updateBoard(board);
        }
        return new ModelAndView("redirect:/showBoard?id=" + boardId);
    }
}
