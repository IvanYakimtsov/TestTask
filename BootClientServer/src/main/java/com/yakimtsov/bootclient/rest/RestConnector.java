package com.yakimtsov.bootclient.rest;

import com.yakimtsov.bootclient.entity.Board;
import com.yakimtsov.bootclient.entity.Stage;
import com.yakimtsov.bootclient.entity.Task;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class RestConnector {
    private static final String SHOW_BOARDS_URL = "http://localhost:8080/trellolo/boards";
    private static final String UPDATE_BOARD_URL = "http://localhost:8080/trellolo/boards/update";
    private static final String ADD_BOARD_URL = "http://localhost:8080/trellolo/boards/add";
    private static final String ADD_TASK_URL = "http://localhost:8080/trellolo/tasks/add";
    private static final String ADD_STAGE_URL = "http://localhost:8080/trellolo/stages/add";
    private static final String DELETE_BOARD_URL = "http://localhost:8080/trellolo/boards/delete/";
    private static final String DELETE_STAGE_URL = "http://localhost:8080/trellolo/stages/delete/";
    private static final String DELETE_TASK_URL = "http://localhost:8080/trellolo/tasks/delete/";
    private static final String URL_DELIMITER = "/";

    public List<Board> findBoards() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ResponseEntity<Board[]> response = restTemplate.getForEntity(SHOW_BOARDS_URL, Board[].class);
        return Arrays.asList(response.getBody());
    }

    public Board findBoard(String id){
        String url = SHOW_BOARDS_URL + URL_DELIMITER + id;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        return restTemplate.getForObject(url,Board.class);
    }

    public void addBoard(Board board){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        restTemplate.put(ADD_BOARD_URL, board, Board.class);
    }

    public void addStage(Stage stage){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        restTemplate.put(ADD_STAGE_URL, stage, Stage.class);
    }

    public void addTask(Task task){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        restTemplate.put(ADD_TASK_URL, task, Task.class);
    }

    public void deleteBoard(String id){
        String url = DELETE_BOARD_URL + id;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(url);
    }

    public void deleteStage(String id){
        String url = DELETE_STAGE_URL + id;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(url);
    }

    public void deleteTask(String id){
        String url = DELETE_TASK_URL + id;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(url);
    }

    public void updateBoard(Board board) {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        restTemplate.postForObject(UPDATE_BOARD_URL, board, Board.class);
    }
}
